## WEB-POC-UI

Este es una aplicación web POC usando [Next.js](https://nextjs.org/) y [React](https://nextjs.org/) creado con el comando[`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

Tecnologías usadas

- TypeScript
- React
- Next

## Instalacion

Primero, clona este repositorio:

```bash
git clone https://gitlab.com/Chasty1/movistar-rfp-poc.git
```

Segundo, ejecuta este comando para instalar las dependencias.

```bash
npm install
# or
yarn install
```

Finalmente, ejecuta el servidor de desarrollo:

```bash
npm run dev
# or
yarn dev
```

Abre [http://localhost:4000](http://localhost:4000) en el navegador para ver la aplicación web.

## Uso

### Rol Retencion

Ingresa con el rol call center en la pantalla de login
con los datos siguientes.

```
- Email: user1
- Contraseña: pass
```

<img height="400"  src="doc/images/login-rol-callcenter.png">

### Rol CallCenter

Ingresa con el rol call center en la pantalla de login
con los datos siguientes.

```
- Email: user2
- Contraseña: pass2
```

<img height="400"  src="doc/images/login-rol-retencion.png">

## Video Demo

![Demo Doccou alpha](doc/images/video-demo.gif)
