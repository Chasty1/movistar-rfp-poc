import React, { createContext, useReducer, useContext, useMemo } from "react";
import reducer from "./combine-reducers";
import { ICustomer, IOffer, IProfile, IRepresentante, IUser } from "./types";
import { _storage } from "../utils/functions";

type IStateType = {
  profile: IProfile;
  user: IRepresentante;
  customer: ICustomer;
  offers: IOffer[];
};

type IContext = {
  state: IStateType;
  dispatch: React.Dispatch<any>;
};

interface IInitialState {
  initialState: IStateType;
}

export default function makeStore() {
  const context = createContext<IContext>({
    state: { profile: {}, user: { token: null }, customer: {}, offers: [] },
    dispatch: () => null,
  });

  const StoreProvider: React.FC<IInitialState> = ({
    children,
    initialState,
  }) => {
    const [state, dispatch] = useReducer(reducer, initialState);
    const contextValues = useMemo(() => ({ state, dispatch }), [state]);

    return (
      <context.Provider value={contextValues}>{children}</context.Provider>
    );
  };

  const useStore = () => useContext(context);

  return { useStore, StoreProvider };
}
