import { IUser, Action } from "../types";

const userReducer = (state: IUser, action: any) => {
  switch(action.type) {
    case Action.SetToken:
      let token = action.payload;
      localStorage.setItem('access_token', JSON.stringify(token));
      return { ...state, token };
    case Action.Reset:
      localStorage.removeItem('access_token');
      localStorage.removeItem('context_ani');
      return { token: null };
    case Action.SetUserInfo:
      return { ...state, ...action.payload }
    default: return state;
  }
};


export default userReducer;