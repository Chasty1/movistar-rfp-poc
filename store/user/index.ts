import { Action, IRepresentante } from "../types";
import { Router, useRouter } from "next/router";
import store from "../../store";
import { useRequestProfile } from "../profile";
import { useSetCustomer } from "../customer";

export function useUser() {
  const {
    state: { user },
  } = store.useStore();
  return user;
}

export function useSetToken() {
  const { dispatch } = store.useStore();

  return (token: string) =>
    dispatch({
      type: Action.SetToken,
      payload: token,
    });
}

export function useSetUserInfo() {
  const { dispatch } = store.useStore();

  return (info: IRepresentante) =>
    dispatch({
      type: Action.SetUserInfo,
      payload: info,
    });
}

export function useUserInfo() {
  useRequestProfile();
  const {
    state: { user },
  } = store.useStore();
  return user;
}

export function useLogout() {
  const { dispatch } = store.useStore();
  const router = useRouter();
  const setCustomer = useSetCustomer();

  return () => {
    setCustomer(null);
    dispatch({ type: Action.Reset });
    router.push("/");
  };
}
