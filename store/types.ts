export type CustomerValue =
  | {
      denominacion: string;
      ver_facturas: boolean;
      monto_a_pagar: boolean;
    }
  | undefined;

export enum LoginType {
  IDP = "IDP",
  MC = "MC",
  TIME = "TIME",
}

export enum Platform {
  WEB = "WEB",
  ANDROID = "ANDROID",
  IOS = "IOS",
}

export enum Segment {
  FULL = "FULL",
  CONTROL = "CONTROL",
  PREPAGO = "PREPAGO",
}

export enum Bussiness {
  B2B = "B2B",
  B2C = "B2C",
}

export enum Product {
  MOVIL = "MOVIL",
  FIJO = "FIJO",
  INTERNET = "INTERNET",
}

export type Identities = {
  id?: number;
  bussiness?: Bussiness;
  product?: Product;
  segment?: Segment;
  plan?: string;
};

export interface IProfile {
  ANI?: number;
  context_ani?: number;
  identities?: Identities[];
  suscriptor?: string;
  bussiness?: string;
  customer_value?: CustomerValue;
  login_type?: string;
  platform?: string;
}

export interface IUser {
  token?: string | null;
  name?: string;
  email?: string;
}

export enum Action {
  SetToken = "SET_TOKEN",
  RequestProfile = "REQUEST_PROFILE",
  Reset = "RESET",
  SelectProduct = "SELECT_PRODUCT",
  SetUserInfo = "SET_USER_INFO",
  SelectAni = "SELECT_ANI",
  SetCustomer = "SET_CUSTOMER",
  SetFinantialAccounts = "SET_FINANTIAL_ACCOUNTS",
  SetSubscribers = "SET_SUBSCRIBERS",
  SetSelectedSubscriber = "SET_SELECTED_SUBSCRIBER",
  SetOffers = "SET_OFFERS",
}

export interface IRepresentante {
  id?: number;
  name?: string;
  rol?: string;
  token?: string;
}

export interface ICustomer {
  id?: number;
  name?: string;
  doc_type?: string;
  doc_number?: string;
  customer_type?: string;
  customer_score?: number;
  finantial_accounts?: IFinantialAccount[];
  subscribers?: ISubscriber[];
}

export interface IFinantialAccount {
  id_fa: string;
  business_line: string;
  status: string;
  balance: number;
}

export interface ISubscriber {
  id: number;
  product_type: string;
  offer_id: number;
  status: string;
  id_fa: string;
  selected: boolean;
}

export interface IOffer {
  id: number;
  name: string;
  product_type: string;
  price: number;
  type: string;
  selected: boolean;
}
