import { Action, ICustomer, IFinantialAccount, ISubscriber } from "../types";
import store from "../../store";
import { useUser } from "../user";
import { useEffect, useRef } from "react";
import getCustomer from "../../service/get-customer";

export function useCustomer() {
  const {
    state: { customer },
  } = store.useStore();
  return customer;
}

export function useSelectedSubscriber() {
  const {
    state: {
      customer: { subscribers },
    },
  } = store.useStore();

  let selectedSubscriber = subscribers?.find((s) => s.selected);

  return selectedSubscriber;
}

export function useSetCustomer() {
  const { dispatch } = store.useStore();

  return (customer: ICustomer) =>
    dispatch({
      type: Action.SetCustomer,
      payload: customer,
    });
}

export function useSetFinantialAccounts() {
  const { dispatch } = store.useStore();

  return (finantial_accounts: IFinantialAccount[]) =>
    dispatch({
      type: Action.SetFinantialAccounts,
      payload: finantial_accounts,
    });
}

export function useSetSubscribers() {
  const { dispatch } = store.useStore();
  return (subscribers: ISubscriber[]) =>
    dispatch({
      type: Action.SetSubscribers,
      payload: subscribers,
    });
}

export function useSetSelectedSubscriber() {
  const { dispatch } = store.useStore();
  return (selected: ISubscriber) =>
    dispatch({
      type: Action.SetSelectedSubscriber,
      payload: selected,
    });
}
