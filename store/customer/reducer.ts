import { IUser, Action, ICustomer } from "../types";

const userReducer = (state: ICustomer, action: any) => {
  switch (action.type) {
    case Action.SetCustomer:
      return { ...action.payload };
    case Action.SetFinantialAccounts:
      return {
        ...state,
        finantial_accounts: action.payload,
      };
    case Action.SetSubscribers:
      return {
        ...state,
        subscribers: action.payload,
      };
    case Action.SetSelectedSubscriber:
      state.subscribers.map((ele, _) => (ele.selected = false));
      action.payload.selected = !action.payload.selected;
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default userReducer;
