import { Action, IProfile } from "../types";
import { _storage } from "../../utils/functions";

const profileReducer = (state: IProfile, action: any) => {
  switch (action.type) {
    case Action.RequestProfile:
      return {
        ...state,
        ...action.payload,
      };

    case Action.SelectAni:
      localStorage.setItem("context_ani", action.payload);
      return { ...state, context_ani: action.payload };

    case Action.Reset:
      return {};
    default:
      return state;
  }
};

export default profileReducer;
