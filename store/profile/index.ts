import { useEffect, useRef } from "react";
import getProfile from "../../service/get-profile";
import store from "../../store";
import { useSetUserInfo, useUser } from "../../store/user";
import { Action, IRepresentante } from "../../store/types";
import { _storage } from "../../utils/functions";
import { getOS } from "../../utils/window";

export function useProfile() {
  useRequestProfile();
  const {
    state: { profile },
  } = store.useStore();
  return profile;
}

export function useContextAni() {
  const {
    state: { profile },
  } = store.useStore();
  const aniFromLocalStorage = _storage.get("context_ani");

  if (aniFromLocalStorage) return aniFromLocalStorage;
  else return profile?.context_ani;
}

// Realizar el request si
// 1. Existe el token luego de iniciar sesión
// 2. No esta la info del perfil en el estado'

export function useRequestProfile() {
  const { token } = useUser();
  const { dispatch } = store.useStore();
  const setUserInfo = useSetUserInfo();
  const {
    state: { profile },
  } = store.useStore();
  const activeRef = useRef<boolean | Promise<void>>(false);

  const _fetch = () => {
    if (!activeRef.current) {
      // dispatch({ type: Action.RequestProfileLoading }); ???
      activeRef.current = (async () => {
        try {
          const data: IRepresentante = await getProfile({ token }).then(
            (res) => res
          );

          /*dispatch({
            type: Action.RequestProfile,
            payload: data,
          });*/

          setUserInfo(data); //hardcode verificar
        } catch (e) {
          console.log(e);
          // dispatch({ type: Action.RequestProfileFail, payload: e }); ???
        } finally {
          activeRef.current = false;
        }
      })();
    }
  };

  useEffect(() => {
    if (Object.keys(profile).length === 0) {
      _fetch();
    }
  }, []);
}

export function useChangeSelectedAni() {
  const { dispatch } = store.useStore();

  return (value: number) =>
    dispatch({
      type: Action.SelectAni,
      payload: value,
    });
}
