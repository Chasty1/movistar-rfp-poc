import { ICustomer, IOffer, IProfile, IUser } from "./types";
import profileReducer from "./profile/reducer";
import userReducer from "./user/reducer";
import customerReducer from "./customer/reducer";
import offerReducer from "./offer/reducer";

type StateType = {
  profile: IProfile;
  user: IUser;
  customer: ICustomer;
  offers: IOffer[];
};

const mainReducer = (
  { profile, user, customer, offers }: StateType,
  action: any
) => ({
  profile: profileReducer(profile, action),
  user: userReducer(user, action),
  customer: customerReducer(customer, action),
  offers: offerReducer(offers, action),
});

export default mainReducer;
