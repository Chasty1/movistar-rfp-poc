import {
  Action,
  ICustomer,
  IFinantialAccount,
  IOffer,
  ISubscriber,
} from "../types";
import store from "../../store";
import { useUser } from "../user";
import { useEffect, useRef } from "react";
import getCustomer from "../../service/get-customer";

export function useOffers() {
  const {
    state: { offers },
  } = store.useStore();
  return offers;
}

export function useSelectedSubscriber() {
  const {
    state: {
      customer: { subscribers },
    },
  } = store.useStore();

  let selectedSubscriber = subscribers?.find((s) => s.selected);

  return selectedSubscriber;
}

export function useSetOffers() {
  const { dispatch } = store.useStore();

  return (offers: IOffer[]) =>
    dispatch({
      type: Action.SetOffers,
      payload: offers,
    });
}
