import { IUser, Action, ICustomer, IOffer } from "../types";

const userReducer = (state: IOffer[], action: any) => {
  switch (action.type) {
    case Action.SetOffers:
      return action.payload;

    default:
      return state;
  }
};

export default userReducer;
