const RESTORE_TOKEN = "RESTORE_TOKEN";
const SIGN_IN = "SIGN_IN";
const SIGN_OUT = "SIGN_OUT";

export const authReducer = (prevState, action) => {
  switch (action.type) {
    case RESTORE_TOKEN:
      return {
        ...prevState,
        userToken: action.token,
      };

    case SIGN_IN:
      return {
        ...prevState,
        id: action.payload.id,
        username: action.payload.username,
        userToken: action.payload.userToken,
      };

    case SIGN_OUT:
      return {
        ...prevState,
        id: 0,
        username: null,
        userToken: null,
      };

    default:
      return prevState;
  }
};
