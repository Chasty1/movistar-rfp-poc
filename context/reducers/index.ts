import { userReducer } from "./userReducer";
import { authReducer } from "./authReducer";

const mainReducer = ({ users, profile }, action) => {
  return {
    users: userReducer(users, action),
    profile: authReducer(profile, action),
  };
};

export default mainReducer;
