const customerData = {
  "OC": {
    denominacion: 'TOP 1 Si',
    ver_facturas: true,
    monto_a_pagar: true
  },
  "OH": {
    denominacion: 'TOP 2 Si',
    ver_facturas: true,
    monto_a_pagar: true
  },
  "OP": {
    denominacion: 'TOP Gobierno AMBA no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "OQ": {
    denominacion: 'TOP GOB. Interior no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "EO": {
    denominacion: 'Corporate REGIONAL no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "EJ": {
    denominacion: 'Licitación EMP Móviles no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "EP": {
    denominacion: 'Coporate Medio Valor no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "EW": {
    denominacion: 'Corporate Global no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "EY": {
    denominacion: 'Intercompany no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "GE": {
    denominacion: 'Coporate Embajada no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "GF": {
    denominacion: 'Coporate Func. GOB. no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "GG": {
    denominacion: 'Coporate GOB. NAC. no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "GL": {
    denominacion: 'Coporate GOB. PROV. no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "GM": {
    denominacion: 'Coporate GOB. MUN. no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "RA": {
    denominacion: 'Carriers no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "RB": {
    denominacion: 'Sitios no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "TP": {
    denominacion: 'Telefonía Pública no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "0D": {
    denominacion: 'Alto Valor si',
    ver_facturas: true,
    monto_a_pagar: true
  },
  "0F": {
    denominacion: 'Alto Valor FM Si',
    ver_facturas: true,
    monto_a_pagar: true
  },
  "0J": {
    denominacion: 'Alto Riesgo Si',
    ver_facturas: true,
    monto_a_pagar: true
  },
  // "ver...": {
  //   denominacion: 'Alto Riesgo FM no',
  //   ver_facturas: false,
  //   monto_a_pagar: false
  // },
  "EX": {
    denominacion: 'Corportate Alto valor no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "ES": {
    denominacion: 'Emp Doble Cuenta ES no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "EA": {
    denominacion: 'Empresa Bajo Valor A no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "EB": {
    denominacion: 'Empresas no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "EC": {
    denominacion: 'Empresa Bajo Valor C no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "ED": {
    denominacion: 'Empresa Bajo Valor D',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "EN": {
    denominacion: 'Empresas Nueva no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "EQ": {
    denominacion: 'Corporate Bajon Valor Nominada no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "ER": {
    denominacion: 'Empresa Bajo Valor',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "EZ": {
    denominacion: 'Empresa LD (CyO) no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "F0": {
    denominacion: 'Profesional 0 (N) Si',
    ver_facturas: true,
    monto_a_pagar: true
  },
  "F1": {
    denominacion: 'Profesional 1 (A) Si',
    ver_facturas: true,
    monto_a_pagar: true
  },
  "F2": {
    denominacion: 'Profesional 2 (B) Si',
    ver_facturas: true,
    monto_a_pagar: true
  },
  "F3": {
    denominacion: 'Profesional 3 (C) Si',
    ver_facturas: true,
    monto_a_pagar: true
  },
  "F4": {
    denominacion: 'Profesional 4 (D) Si',
    ver_facturas: true,
    monto_a_pagar: true
  },
  "FA": {
    denominacion: 'Empresa FA no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "FB": {
    denominacion: 'Empresa FB no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "FC": {
    denominacion: 'Empresa FC no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "FD": {
    denominacion: 'Empresa FD no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "FF": {
    denominacion: 'Profesional Riesto Financiero no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "FN": {
    denominacion: 'Empresa FN no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "TN": {
    denominacion: 'Telefónica Negocios no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "FZ": {
    denominacion: 'Profesional LD (CyO) si',
    ver_facturas: true,
    monto_a_pagar: true
  },
  "0R": {
    denominacion: 'Competencia  no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "0G": {
    denominacion: 'Competencia FM no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "0I": {
    denominacion: 'Masivos no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "0E": {
    denominacion: 'Nuevos Negocios si',
    ver_facturas: true,
    monto_a_pagar: true
  },
  "MC": {
    denominacion: 'Canje Corporate no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "ML": {
    denominacion: 'Canje Vip Corporate no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "MO": {
    denominacion: 'Canje Corporate O no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "EE": {
    denominacion: 'Corporate Perímetro no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "EL": {
    denominacion: 'Licitación Emp. Perímet 1 no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "EF": {
    denominacion: 'Corporate Perímetro 1 no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "EG": {
    denominacion: 'Corporate Perímetro 2 no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "MT": {
    denominacion: 'Canje Empresas no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  // "TN (duplicado)": {
    //   denominacion: 'Telefonica Negocios (ONG) n',
    //   ver_facturas: false,
    //   monto_a_pagar: false
    // },
  "EK": {
    denominacion: 'Emp Doble Cuenta EK no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "0K": {
    denominacion: 'TEmpresas II Priv no',
    ver_facturas: false,
    monto_a_pagar: false
  },
  "0A": {
    denominacion: 'Prestador no',
    ver_facturas: false,
    monto_a_pagar: false
  },
    

}

export default customerData;