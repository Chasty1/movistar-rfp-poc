export enum LoginType {
  IDP = 'IDP',
  MC = 'MC',
  TIME = 'TIME'
}

export enum Platform {
  WEB = 'WEB',
  ANDROID = 'ANDROID',
  IOS = 'IOS'
}

export enum Segment {
  FULL = 'FULL',
  CONTROL = 'CONTROL',
  PREPAGO = 'PREPAGO'
}

export enum Bussiness {
  B2B = 'B2B',
  B2C = 'B2C'
}

export enum Product {
  MOVIL = 'MOVIL',
  FIJO = 'FIJO',
  INTERNET = 'INTERNET'
}


export interface IUser {
  username: string,
  id: number,
  email: string,
  password: string,
  name: string
}


export type CustomerValue = {
  denominacion: string,
  ver_facturas: boolean,
  monto_a_pagar: boolean
} | undefined
