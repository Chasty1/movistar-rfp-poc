import CUSTOMER_DATA from './customer_value-data';
import {
  Bussiness,
  LoginType,
  Platform,
  Segment,
  CustomerValue,
  Product
} from './types';

export interface IProfile {
  suscriptor?: null,
  email?: string,
  ANI?: number,
  bussiness?: Bussiness,
  product?: string[],
  segment?: Segment,
  customer_value?: CustomerValue,
  plan?: string,
  login_type?: LoginType,
  platform?: string,
  context_ani?: string
}

export const __mock_profile__ = [
  {
  "id": 1,
  "name": "Squad Apk",
  "email": "squad.apk@telefonica.com",
  "context_ani": 5413332123134,
  "customer_value": "",
  "bussiness": Bussiness.B2C,
  "suscriptor": '',
  "identities": [
      {
          "product": Product.MOVIL,
          "segment": Segment.CONTROL,
          "bussiness": Bussiness.B2C,
          "plan": '5GB', 
          "id": 5413332123134,
      },
      {
        "product": Product.MOVIL,
        "segment": Segment.PREPAGO,
        "bussiness": Bussiness.B2C,
        "plan": '',
        "id": 541869492123,
      },
      {
        "product": Product.MOVIL,
        "segment": Segment.PREPAGO,
        "bussiness": Bussiness.B2C,
        "plan": '',
        "id": 541869436643,
      },
      {
        "product": Product.MOVIL,
        "segment": Segment.FULL,
        "bussiness": Bussiness.B2C,
        "plan": '10GB',
        "id": 541555667123,
      },
      {
        "product": Product.MOVIL,
        "segment": Segment.PREPAGO,
        "bussiness": Bussiness.B2C,
        "plan": '',
        "id": 5418694377743,
      },
    ]
  },
  {
    "id": 2,
    "name": "Demo Apk",
    "email": "demo@mail.com",
    "context_ani": 541886949333,
    "customer_value": CUSTOMER_DATA['0A'],
    "bussiness": Bussiness.B2B,
    "suscriptor": '',
    "identities": [
        {
            "product": Product.MOVIL,
            "segment": Segment.CONTROL,
            "bussiness": Bussiness.B2B,
            "plan": '3GB', 
            "id": 541886949333,
        },
        {
          "product": Product.MOVIL,
          "segment": Segment.FULL,
          "bussiness": Bussiness.B2B,
          "plan": '10GB',
          "id": 541869452123,
        },
        {
          "product": Product.MOVIL,
          "segment": Segment.PREPAGO,
          "bussiness": Bussiness.B2B,
          "plan": '',
          "id": 543305981312,
        },
        {
          "product": Product.MOVIL,
          "segment": Segment.PREPAGO,
          "bussiness": Bussiness.B2B,
          "plan": '',
          "id": 543231203451,
        },
        {
          "product": Product.MOVIL,
          "segment": Segment.CONTROL,
          "bussiness": Bussiness.B2B,
          "plan": '5GB',
          "id": 54775843991,
        },
      ]
    },

]


