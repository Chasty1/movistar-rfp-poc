import { off } from "process";
import React, { useEffect, useState } from "react";
import { useFetchReplaceOffer } from "../../hooks/useOffer";
import { useSelectedSubscriber } from "../../store/customer";
import { useOffers } from "../../store/offer";
import BoxWrapper from "./box-wrapper";
import Button from "./button";
import styles from "./styles/pop-up.module.css";

function PopUp({ open, setOpen, offerClicked }) {
  const [selectedIndex, setSelectedIndex] = useState(-1);
  const offers = useOffers();
  const { saveOffer, isLoading, message, setMessage } = useFetchReplaceOffer();
  const selectedSubscriber = useSelectedSubscriber();

  const [title, setTitle] = useState("");

  useEffect(() => {
    if (message) {
      setOpen(false);
      offerClicked(message);
      setSelectedIndex(-1);
      setMessage(null);
    }
  }, [message]);

  useEffect(() => {
    if (offers && selectedSubscriber) {
      const filteredOffers = offers.filter(
        (o) =>
          o.product_type == selectedSubscriber.product_type &&
          o.id != selectedSubscriber.offer_id
      );

      if (filteredOffers.length == 0) {
        setTitle(
          `No existen otros planes para - ${selectedSubscriber?.product_type}`
        );
        setTimeout(() => {
          setOpen(false);
        }, 1500);
      } else {
        setTitle(`Cambio de plan - ${selectedSubscriber?.product_type}`);
      }
    }
  }, [offers, selectedSubscriber]);

  if (!open) return null;
  return (
    <div
      onClick={(e) => {
        setOpen(false);
        setSelectedIndex(-1);
      }}
      className={styles.container}
    >
      <div
        className={styles.boxWrapper}
        onClick={(e) => {
          e.stopPropagation();
        }}
      >
        <h1 style={{ fontSize: 18 }}>{title}</h1>
        {offers
          .filter(
            (o) =>
              o.product_type == selectedSubscriber.product_type &&
              o.id != selectedSubscriber.offer_id
          )
          ?.map((ele, index) => (
            <div
              className={[
                styles.plan,
                selectedIndex == index ? styles.selected : "",
              ].join(" ")}
              onClick={() => {
                setSelectedIndex(index);
              }}
            >
              <span>{ele.name}</span>
              <span>{ele.product_type}</span>
              <span>$ {ele.price}</span>
            </div>
          ))}
        {selectedIndex >= 0 && (
          <Button
            text="Continuar"
            onClick={() => {
              saveOffer();
            }}
          />
        )}
      </div>
    </div>
  );
}

export default PopUp;
