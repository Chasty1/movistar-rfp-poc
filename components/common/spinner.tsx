import React from "react";
import styles from "./styles/spinner.module.css";

function Spinner(props: { color?: string }) {
  return (
    <svg
      aria-labelledby="aria-id-hook-1"
      className={styles.container}
      height="32"
      role="img"
      viewBox="0 0 66 66"
      width="32"
    >
      <title>Cargando</title>
      <circle
        className={styles.spinner}
        cx="33"
        cy="33"
        fill="none"
        r="30"
        role="presentation"
        stroke={props.color ? props.color : "#5BC500"}
        strokeLinecap="square"
        strokeWidth="6"
      />
    </svg>
  );
}

export default Spinner;
