import React from "react";
import styles from "./styles/sub-title.module.css";

type SubTitleProps = {
  text: string;
};

function SubTitle({ text }: SubTitleProps) {
  return <h2 className={styles.loginSubtitle}>{text}</h2>;
}

export default SubTitle;
