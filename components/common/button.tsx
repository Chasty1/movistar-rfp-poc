import React from "react";
import Spinner from "./spinner";
import styles from "./styles/button.module.css";
import { EventObject, pushGTMEvent } from "../../utils/analytics/gtm";

function Button(props: {
  inverted?: boolean;
  onClick: Function;
  text: string;
  className?: string;
  style?: any;
  loading?: boolean;
  disabled?: boolean;
  gtmObject?: EventObject;
}) {
  let className: string, style: object;

  if (props.className) {
    className = styles.button + " " + props.className;
  } else {
    className = styles.button;
  }

  if (props.style) {
    style = props.style;
  } else {
    style = {};
  }

  const handleClick = () => {
    if (props.gtmObject) pushGTMEvent(props.gtmObject);
    props.onClick();
  };
  return (
    <button
      onClick={handleClick}
      style={style}
      disabled={props.disabled}
      className={className}
    >
      {props.loading ? <Spinner color="#ffffff" /> : props.text}
    </button>
  );
}
export default Button;
