import React from "react";
import styles from "./styles/tab-item.module.css";

type TabItemProps = {
  text: string;
  onClick: () => void;
  selected?: boolean;
};

function TabItem({ text, onClick, selected }: TabItemProps) {
  return (
    <li
      className={[
        styles.loginTabsItem,
        selected ? styles.loginTabsItemSelected : "",
      ].join(" ")}
    >
      <a onClick={onClick}>{text}</a>
    </li>
  );
}

export default TabItem;
