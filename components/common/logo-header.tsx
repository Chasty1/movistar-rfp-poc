import React from "react";
import styles from "./styles/logo-header.module.css";

function LogoHeader() {
  return (
    <div className={styles.header}>
      <div className={styles.headerImg}>
        <img src="/assets/images/imagotype.svg" alt="" />
      </div>
    </div>
  );
}

export default LogoHeader;
