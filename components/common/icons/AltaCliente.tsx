import React from 'react'

const AltaCliente = () => {
    return (
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 512 512">
            <path fill="#fff" d="M288 128l-64-64h-224v416h512v-352h-224zM352 352h-64v64h-64v-64h-64v-64h64v-64h64v64h64v64z"></path>
        </svg>
    )
}

export default AltaCliente