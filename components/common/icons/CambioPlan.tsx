import React from 'react'

const CambioPlan = () => {
    return (
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 512 512">
            <path fill="#fff" d="M0 24v192c0 13.2 7.637 31.637 16.971 40.971l238.058 238.058c9.334 9.334 24.607 9.334 33.941 0l206.059-206.059c9.334-9.334 9.334-24.607 0-33.941l-238.059-238.059c-9.333-9.333-27.77-16.97-40.97-16.97h-192c-13.2 0-24 10.8-24 24zM192 144c0 26.51-21.49 48-48 48s-48-21.49-48-48 21.49-48 48-48 48 21.49 48 48z"></path>
        </svg>
    )
}

export default CambioPlan