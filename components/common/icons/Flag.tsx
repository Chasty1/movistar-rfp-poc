import React from 'react'

const Flag = () => {
    return (
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 512 512">
            <path fill="#fff" d="M96 0v512l160-160 160 160v-512z"></path>
        </svg>
    )
}

export default Flag