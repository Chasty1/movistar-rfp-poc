import { Head } from "next/document";
import React, { useEffect, useState } from "react";
import Content from "./content";
import GlobalWrapper from "./global.wrapper";
import Navbar from "./navbar";

type MainAppProps = {
  children?: any;
};

export default function MainAppContainer(props: MainAppProps) {
  return (
    <GlobalWrapper>
      <Navbar />
      <Content>{props.children}</Content>
    </GlobalWrapper>
  );
}
