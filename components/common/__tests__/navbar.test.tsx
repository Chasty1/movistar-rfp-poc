/**
 * @jest-environment jsdom
 */
import React from "react";
import { shallow } from "enzyme";
import Navbar from "../navbar";
import Link from "next/link";
import NavItem from "../nav-item";
import { getOS } from "../../../utils/window";
var jsdom = require("jsdom");

it("Navbar has movistar logo image", () => {
  const wrapper = shallow(<Navbar />);
  expect(wrapper.find(Link).children().first().type()).toEqual("img");
});

it("Navbar has mainNavContainer className", () => {
  const wrapper = shallow(<Navbar />);
  expect(wrapper.props().className).toContain("mainNavContainer");
});

it("Navbar has mainNavTabsContainer className", () => {
  const wrapper = shallow(<Navbar />);
  expect(wrapper.children().first().props().className).toEqual(
    "mainNavTabsContainer"
  );
});

it("Navbar has mainNavBottomTabsContainer className", () => {
  const wrapper = shallow(<Navbar />);
  expect(wrapper.children().last().props().className).toEqual(
    "mainNavBottomTabsContainer"
  );
});

it("Navbar has custom className", () => {
  const wrapper = shallow(<Navbar className="Foo" />);
  expect(wrapper.props().className).toEqual("mainNavContainer Foo");
});

it("Navbar renders five <NavItem /> components", () => {
  const wrapper = shallow(<Navbar />);
  expect(wrapper.find(NavItem).length).toEqual(5);
});

describe("Navbar Test", () => {
  let props;
  let wrapper;
  let useEffect;

  let mockUseEffect = () => {
    useEffect.mockImplementationOnce((f) => f());
  };

  const OS = "MAC OS";
  const window = {
    navigator: {
      userAgent: "",
      platform: "Macintosh",
    },
  };

  beforeEach(() => {
    useEffect = jest.spyOn(React, "useEffect");
    props = {
      getOS: jest.fn().mockResolvedValue(getOS(window)),
    };

    props.getOS();
    mockUseEffect();

    wrapper = shallow(<Navbar />);
  });

  describe("on start", () => {
    it("get OS", () => {
      expect(props.getOS).toHaveBeenCalled();
    });
  });
});
