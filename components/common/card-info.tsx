import React from "react";
import styles from "./styles/card-info.module.css";

type CardInfoProps = {
  title: string;
  date: string;
  fechaFactura: string;
  deuda: string;
};

function CardInfo({ title, date, fechaFactura, deuda }: CardInfoProps) {
  return (
    <div className={styles.container}>
      <div className={styles.cardContainer}>
        <p>{title}</p>
        <p className={styles.date}>{date}</p>
        <div className={styles.separator}></div>
        <span style={{ fontSize: 11 }}>{fechaFactura}</span>
        <p style={{ fontSize: 11 }}>
          Monto adeudado <span>${deuda}</span>
        </p>
      </div>
      <div className={styles.buttonsContainer}>
        <span className={styles.buttonLabel}>Ver detalle</span>
        <span className={styles.buttonLabel}>Historicos</span>
      </div>
    </div>
  );
}

export default CardInfo;
