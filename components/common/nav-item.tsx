import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import styles from "./styles/navbar.module.css";

export const NavItem = (props: {
  className?: string;
  tabText: string;
  href: string;
  iconPathOn: string;
  iconPathOff: string;
}) => {
  const router = useRouter();
  return (
    <li className={[styles.mainNavTabItem, props.className].join(" ")}>
      <Link href={props.href}>
        <a className={styles.mainNavTab}>
          <img
            className={styles.mainNavTabIcon}
            src={
              props.href === router.pathname
                ? props.iconPathOn
                : props.iconPathOff
            }
          />
          <p
            className={
              props.href === router.pathname
                ? styles.mainNavTabTextSelected
                : styles.mainNavTabText
            }
          >
            {props.tabText}
          </p>
        </a>
      </Link>
    </li>
  );
};

export default NavItem;
