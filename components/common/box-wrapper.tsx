import React from "react";
import styles from "./styles/box-wrapper.module.css";

function BoxWrapper({ children }) {
  return (
    <div className={styles.container}>
      <div className={styles.innerContainer}>
        <div className={styles.innerContainerBox}>{children}</div>
      </div>
    </div>
  );
}

export default BoxWrapper;
