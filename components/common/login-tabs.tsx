import React from "react";
import styles from "./styles/login-tabs.module.css";

function LoginTabs({ children }) {
  return <ul className={styles.loginTabs}>{children}</ul>;
}

export default LoginTabs;
