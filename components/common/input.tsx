import React from "react";
import styles from "./styles/input.module.css";

type InputProps = {
  id: string;
  placeholder: string;
  type: string;
  hasIcon?: boolean;
  iconBgColor?: string;
  iconPath?: string;
  bottomLabel?: string;
  name: string;
  onChange: (e) => void;
  onClickIcon?: (e) => void;
};

function Input({
  id,
  placeholder,
  type,
  hasIcon,
  iconBgColor,
  iconPath,
  bottomLabel,
  name,
  onChange,
  onClickIcon,
}: InputProps) {
  return (
    <div className={[styles.formItem, styles.formItemText].join(" ")}>
      <input
        className={[styles.formInput, styles.formInputText].join(" ")}
        type={type}
        placeholder={placeholder}
        aria-required="true"
        id={id}
        name={name}
        onChange={(e) => onChange(e)}
      />
      {hasIcon && (
        <span
          onClick={onClickIcon}
          className={styles.passwordIcon}
          style={{
            backgroundColor: iconBgColor ? iconBgColor : null,
            backgroundImage: `url(${iconPath})`,
          }}
        ></span>
      )}
      <div id="error-password" style={{ display: "none" }}>
        <span className="form__error" style={{ display: "none" }}>
          El número de teléfono no es válido.{" "}
        </span>
      </div>
      {bottomLabel && <h2 className={styles.loginSubtitle2}>{bottomLabel}</h2>}
    </div>
  );
}

export default Input;
