import React, { useCallback, useEffect, useState } from "react";
import {
  useSelectedSubscriber,
  useSetSelectedSubscriber,
  useSetSubscribers,
} from "../../store/customer";
import {
  useProfile,
  useContextAni,
  useChangeSelectedAni,
} from "../../store/profile";
import { ISubscriber } from "../../store/types";
import DropDownItem from "./drop-down-item";
import OutsideAlerter from "./outside-alerter";
import styles from "./styles/drop-down.module.css";

type DropDownProps = {
  subscribers: ISubscriber[];
};

function DropDown({ subscribers }: DropDownProps) {
  const [open, setOpen] = useState(false);
  const setSelectedSubscriber = useSetSelectedSubscriber();
  const selectedSubscriber = useSelectedSubscriber();

  return (
    <div className={styles.dropdown}>
      <DropDownItem
        handleClick={() => subscribers && setOpen(true)}
        leftIcon={"assets/icons/dropdown/icn-mobile.svg"}
        phone={`${selectedSubscriber ? selectedSubscriber.id : "Sin datos"}`}
        name={"Linea telefonica"}
        icon={"assets/icons/dropdown/icn-arrow-down.svg"}
        style={{
          backgroundColor: "#333",
          display: "flex",
          alignItems: "center",
          borderRadius: 5,
        }}
        hasParent={false}
      />
      {open && subscribers && (
        <OutsideAlerter setOpen={setOpen}>
          <div className={styles.dropdownContent}>
            {subscribers.map((ele, index) => (
              <DropDownItem
                key={index}
                handleClick={() => {
                  setOpen(false);
                  setSelectedSubscriber(ele);
                }}
                leftIcon={"assets/icons/dropdown/icn-mobile.svg"}
                phone={`${ele.id}`}
                name={`Línea ${"producto"} - ${"segmento"}`}
                icon={"assets/icons/dropdown/icn-arrow.svg"}
                selected={ele.selected}
                hasParent={true}
              />
            ))}
          </div>
        </OutsideAlerter>
      )}
    </div>
  );
}

export default DropDown;
