import React from 'react'
import TagIcon from '../../../components/common/icons/TagIcon'
import styles from './css/promotions.module.css'

const PromotionsButton = (props : { color : string, text : string }) => {

    const { color, text } = props;

    return (
        <div className={`${styles.button_container} ${styles[color]}`}>
            <TagIcon />
            <button className={styles.button_promotions}>{text}</button>
        </div>
    )
}

export default PromotionsButton