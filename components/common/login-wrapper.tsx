import React from "react";
import styles from "./styles/login-wrapper.module.css";
function LoginWrapper({ children }) {
  return <div className={styles.loginWrapper}>{children}</div>;
}

export default LoginWrapper;
