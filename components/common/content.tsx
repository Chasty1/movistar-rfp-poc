import React, { useEffect, useState } from "react";
import styles from "./styles/content.module.css";

function Content(props) {
  return <div className={styles.content}>{props.children}</div>;
}

export default Content;
