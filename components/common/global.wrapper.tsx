import React, { useEffect, useState } from "react";
import styles from "./styles/global-wrapper.module.css";

function GlobalWrapper(props) {
  return <div className={styles.globalWrapper}>{props.children}</div>;
}

export default GlobalWrapper;
