import React from "react";
import styles from "./styles/login-header.module.css";

type LoginHeaderProps = {
  title: string;
};

function LoginHeader({ title }: LoginHeaderProps) {
  return (
    <header className={styles.loginHeader}>
      <h1 className={styles.loginHeaderTitle}>{title}</h1>
    </header>
  );
}

export default LoginHeader;
