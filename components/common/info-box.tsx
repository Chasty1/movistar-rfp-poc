import React from "react";
import styles from "./styles/info-box.module.css";

type InfoBoxProps = {
  label?: string;
  text?: string;
  iconPath: string;
  iconBgColor: string;
  style?: any;
  rightButton?: string;
};

function InfoBox({
  label,
  text,
  iconPath,
  iconBgColor,
  style,
  rightButton,
}: InfoBoxProps) {
  return (
    <div className={styles.container} style={style}>
      {label && <p>{label}</p>}
      <div className={styles.box}>
        <div
          className={text ? styles.iconContainer : styles.iconContainerRounded}
          style={{ backgroundColor: iconBgColor }}
        >
          <img src={iconPath} alt="" />
        </div>
        {text && (
          <div className={styles.textContainer}>
            <h5 style={{ margin: 0, padding: 0 }}>{text}</h5>
          </div>
        )}
        {rightButton && (
          <div className={styles.rightButton}>
            <span style={{ padding: "0px 16px" }}>{rightButton}</span>
          </div>
        )}
      </div>
    </div>
  );
}

export default InfoBox;
