import React, { useEffect, useState } from "react";
import Link from "next/link";
import styles from "./styles/navbar.module.css";
import NavItem from "./nav-item";
import { getOS } from "../../utils/window";

function Navbar(props: { className?: string }) {
  const [href, setHref] = useState("");
  useEffect(() => {
    const OS = getOS(window);
    const url =
      OS === "Mac OS"
        ? "https://apps.apple.com/ar/app/mi-movistar-argentina/id952347964"
        : "https://play.google.com/store/apps/details?id=com.services.movistar.ar&hl=es_AR";
    setHref(url);
  }, []);
  return (
    <div className={[styles.mainNavContainer, props.className].join(" ")}>
      <ul className={styles.mainNavTabsContainer}>
        <li
          className={[styles.mainNavTabItemLogo, styles.mainNavHomeTab].join(
            " "
          )}
        >
          <Link href="/">
            <img
              className={styles.logo}
              src={"/assets/icons/icn-brand-movistar.svg"}
            />
          </Link>
        </li>
        <NavItem
          href="/home"
          tabText="Home"
          iconPathOn="/assets/icons/navbar/home-on.svg"
          iconPathOff="/assets/icons/navbar/home-off.svg"
        />

        <NavItem
          href="/profile"
          tabText="Perfil"
          iconPathOn="/assets/icons/navbar/icn-nav-profile-on.svg"
          iconPathOff="/assets/icons/navbar/icn-nav-profile-off.svg"
        />
      </ul>
    </div>
  );
}
export default Navbar;
