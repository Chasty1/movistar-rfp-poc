import { useRouter } from "next/router";
import React, { useEffect, useReducer, useState } from "react";
import { useStateValue } from "../../context/state";
import useLogin from "../../hooks/useLogin";
import { users } from "../../mockup";
import ActionButton from "./action-button";
import BoxWrapper from "./box-wrapper";
import ContainerAside from "./container-aside";
import FormLogin from "./form-login";
import InnerContent from "./inner-content";
import Input from "./input";
import LinkText from "./link-text";
import LoginHeader from "./login-header";
import LoginTabs from "./login-tabs";
import LoginWrapper from "./login-wrapper";
import LogoHeader from "./logo-header";
import styles from "./styles/login.module.css";
import SubTitle from "./sub-title";
import TabItem from "./tab-item";
import decodeToken from "jwt-decode";
import { _storage } from "../../utils/functions";

function Login() {
  const [inputValues, setInputValues] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    { username: "", password: "" }
  );
  const { handleLogin, error, isLoading } = useLogin({ ...inputValues });

  const [selectedTab, setSelectedTab] = useState("con contraseña");

  const handleOnChange = (event) => {
    const { name, value } = event.target;
    setInputValues({ ...inputValues, [name]: value });
  };

  return (
    <LoginWrapper>
      <LogoHeader />
      <BoxWrapper>
        <InnerContent>
          <LoginHeader title={"Identificate"} />
          <LoginTabs>
            <TabItem
              selected={selectedTab == "con celular"}
              text="Con Tu celular"
              onClick={() => null /*setSelectedTab("con celular")*/}
            />
            <TabItem
              selected={selectedTab == "con contraseña"}
              text="Con contraseña"
              onClick={() => setSelectedTab("con contraseña")}
            />
          </LoginTabs>

          <FormLogin>
            <p>Accedé con tu número de celular para gestionar esta línea.</p>

            {selectedTab === "con contraseña" ? (
              <>
                <Input
                  id="username"
                  onChange={(e) => handleOnChange(e)}
                  type="text"
                  placeholder="Email"
                  name="username"
                />

                <Input
                  id="password"
                  onChange={(e) => handleOnChange(e)}
                  hasIcon
                  type="password"
                  placeholder="Contraseña"
                  name="password"
                  iconPath="/assets/icons/icn_eye_disabled.svg"
                />
              </>
            ) : (
              <>
                <Input
                  id="otro"
                  type="tel"
                  name="otro"
                  onChange={(e) => {}}
                  placeholder="Ingresá tu teléfono con código de área"
                  bottomLabel="Ingresá tu número de celular, con código de área, sin 0 y sin 15"
                />
              </>
            )}

            {error && <p style={{ color: "red", fontSize: 12 }}>{error}</p>}

            <div className={styles.formButtons}>
              <ActionButton
                id="btn-ingresar"
                onClick={handleLogin}
                label="Acceder"
              />
            </div>
            <p>
              Al continuar aceptás nuestros{" "}
              <LinkText text="términos y condiciones" href="" />
            </p>
          </FormLogin>
        </InnerContent>

        <ContainerAside>
          {selectedTab === "con contraseña" && (
            <>
              <SubTitle text={"¿No tenés tu usuario?"} />
              <p>
                <ActionButton
                  id="btn-ingresar"
                  onClick={() => null}
                  label="Registrate Ahora"
                />
              </p>
              <p>
                <LinkText text="¿Tenés dudas para registrarte?" href="" />
              </p>
            </>
          )}
        </ContainerAside>
      </BoxWrapper>
    </LoginWrapper>
  );
}

export default Login;
