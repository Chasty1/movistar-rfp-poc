import React from "react";
import styles from "./styles/actionsbuttons.module.css";

const ActionsButtons = (props: {
  text: string;
  icon: Function;
  onClick: (e) => void;
}) => {
  const { text, icon } = props;

  return (
    <button onClick={(e) => props.onClick(e)} className={styles.button_actions}>
      <div className={styles.main_container}>
        <div className={styles.icon_container}>{icon()}</div>
        <div className={styles.text_button}>{text}</div>
      </div>
    </button>
  );
};

export default ActionsButtons;
