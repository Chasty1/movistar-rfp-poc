import React from "react";
import styles from "./styles/status-info.module.css";

type StatusInfoProps = {
  label: string;
  value: string;
  active?: boolean;
};

function StatusInfo({ label, value, active }: StatusInfoProps) {
  return (
    <div style={{ display: "flex", alignItems: "center", margin: "0px 16px" }}>
      <p style={{ margin: 0, fontSize: 13 }}>{label}</p>
      <span
        style={{
          background: active ? "#5FC705" : "red",
          color: "#fff",
          padding: "5px 10px",
          borderRadius: 8,
          marginLeft: 10,
          fontSize: 12,
        }}
      >
        {value}
      </span>
    </div>
  );
}

export default StatusInfo;
