import React from "react";
import styles from "./styles/show-alert.module.css";
function ShowAlert({ message, show }) {
  if (!show) return null;
  return (
    <div className={styles.container}>
      <p style={{ color: "white" }}>{message}</p>
    </div>
  );
}

export default ShowAlert;
