import React from "react";
import styles from "./styles/search-bar.module.css";

type SearchBarProps = {
  id: string;
  placeholder: string;
  type: string;
  iconBgColor?: string;
  iconPath?: string;
  name: string;
  onChange: (e) => void;
  onClickIcon?: (e) => void;
};

function SearchBar({
  id,
  placeholder,
  type,
  iconBgColor,
  iconPath,
  name,
  onChange,
  onClickIcon,
}: SearchBarProps) {
  return (
    <div className={[styles.formItem, styles.formItemText].join(" ")}>
      <input
        className={[styles.formInput, styles.formInputText].join(" ")}
        type={type}
        placeholder={placeholder}
        aria-required="true"
        id={id}
        name={name}
        onChange={(e) => onChange(e)}
      />

      <img
        onClick={onClickIcon}
        className={styles.passwordIcon}
        src={iconPath}
        style={{
          backgroundColor: iconBgColor ? iconBgColor : null,
        }}
      />
    </div>
  );
}

export default SearchBar;
