import React from 'react'
import styles from './styles/pelochos.module.css'

const Pelochos = (props : { 
    value : number, 
    text_primary? : string
    text_secondary : string
    text_total_plan? : string
    text_footer : string
}) => {

    const { value, text_primary, text_total_plan, text_secondary, text_footer } = props
    const MAX : number = 139.23007675795088

    let porcentaje = MAX * (value / 100)

    let color = (text_secondary === 'Ilimitado') ? '#0098d2' : (value >= 75) ? '#e40256' : '#0098d2'

    return (
        <div className={styles.imageContainer} >	
            <svg className={styles.circle} xmlns="http://www.w3.org/2000/svg"  height="150" width="150" role="presentation" viewBox="0 0 48 48">
                <circle cx="24" cy="24" r="22" fill="none" stroke="#EEEEEE" strokeWidth="1" strokeDasharray={`${MAX}`} strokeDashoffset="0" strokeLinecap="round" transform="rotate(-90, 24, 24)"></circle>
            </svg>
            <svg className={styles.circle} xmlns="http://www.w3.org/2000/svg"  height="150" width="150" role="presentation" viewBox="0 0 48 48">
                <circle cx="24" cy="24" r="22" fill="none" stroke={`${color}`} strokeWidth="1" strokeDasharray={`${porcentaje}, ${MAX}`} strokeDashoffset="1.478829384871176" strokeLinecap="round" transform="rotate(-90, 24, 24)"></circle>
            </svg>
            <div className={styles.icon}>
                {(text_primary) && <div className={styles.a1}>{text_primary}</div>}
                <div className={styles.a2}>{text_secondary}</div>
                {(text_total_plan) && <div className={styles.a1}>{text_total_plan}</div>}
                <br /><div className={styles.a3}>{text_footer}</div>
            </div>
        </div>
    )
}

export default Pelochos