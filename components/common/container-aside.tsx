import React from "react";
import styles from "./styles/container-aside.module.css";

type AsideProps = {
  children?: any;
};

function ContainerAside({ children }: AsideProps) {
  return <div className={styles.innerContainerAside}>{children}</div>;
}

export default ContainerAside;
