import React from "react";
import styles from "./styles/action-button.module.css";

type ActionButtonProps = {
  id: string;
  onClick: () => any;
  label: string;
};

function ActionButton({ id, onClick, label }: ActionButtonProps) {
  return (
    <button className={styles.action} id={id} onClick={onClick}>
      <span className={styles.actionText}>{label}</span>
    </button>
  );
}

export default ActionButton;
