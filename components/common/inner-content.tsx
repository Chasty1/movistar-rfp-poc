import React from "react";
import styles from "./styles/inner-content.module.css";
function InnerContent({ children }) {
  return <div className={styles.innerContainerContent}>{children}</div>;
}

export default InnerContent;
