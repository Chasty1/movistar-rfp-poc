import React from "react";
import styles from "./styles/drop-down-item.module.css";

const RightIcon = ({ icon, onClick, style }) => {
  return (
    <div style={style}>
      <img
        onClick={() => null}
        className={styles.rightIcon}
        src={icon}
        alt=""
      />
    </div>
  );
};

const LeftIcon = ({ icon }) => {
  return (
    <div className={styles.phoneIcon}>
      <img src={icon} alt="" />
    </div>
  );
};

function DropDownItem(props: {
  phone: string;
  leftIcon: string;
  icon?: string;
  name: string;
  style?: any;
  selected?: boolean;
  hasParent?: boolean;
  handleClick: () => void;
}) {
  return (
    <div className={styles.container} onClick={props.handleClick}>
      <div className={styles.phoneContainer}>
        <span style={{ fontWeight: "bold" }}>ANI</span>
        <div className={styles.phoneTextContainer}>
          <div className={styles.phone}>{props.phone}</div>
        </div>
      </div>

      {!props.hasParent && (
        <RightIcon style={props.style} onClick={() => null} icon={props.icon} />
      )}

      {props.selected && (
        <RightIcon style={props.style} onClick={() => null} icon={props.icon} />
      )}
    </div>
  );
}

export default DropDownItem;
