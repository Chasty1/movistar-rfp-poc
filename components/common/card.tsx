import React from "react";
import styles from "./styles/card.module.css";

type CardProps = {
  children: any;
  style?: any;
};

function Card({ children, style }: CardProps) {
  return (
    <div className={styles.container} style={style}>
      {children}
    </div>
  );
}

export default Card;
