import React from "react";
type LinkTextProps = {
  text: string;
  href: string;
};

function LinkText({ text, href }: LinkTextProps) {
  return (
    <a style={{ color: "#00A9E0", textDecoration: "none" }} href={href}>
      {text}
    </a>
  );
}

export default LinkText;
