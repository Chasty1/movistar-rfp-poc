import React from "react";
import styles from "./styles/form-login.module.css";

function FormLogin({ children }) {
  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
      }}
      className={[styles.form, styles.formTabs].join(" ")}
    >
      {children}
    </form>
  );
}

export default FormLogin;
