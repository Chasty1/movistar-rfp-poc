import { useState, useRef, useEffect } from "react";
import login, { ILogin } from "../service/login";
import { useRouter } from "next/router";
import { useSetToken, useUser } from "../store/user";
import getCustomer from "../service/get-customer";
import {
  useCustomer,
  useSetCustomer,
  useSetFinantialAccounts,
  useSetSubscribers,
} from "../store/customer";
import getFinantialAccounts from "../service/get-finantial-accounts";
import getSubscribers from "../service/get-subscribers";
import { ISubscriber } from "../store/types";
import getOffers from "../service/get-offers";
import { useSetOffers } from "../store/offer";

function useHookCustomer({ search }) {
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const customer = useCustomer();
  const setCustomer = useSetCustomer();
  const setFinantialAccounts = useSetFinantialAccounts();
  const setSubscribers = useSetSubscribers();

  const router = useRouter();
  const { token } = useUser();
  let cancel = useRef(false);

  useEffect(() => {
    cancel.current = false;
  }, [search]);

  const searchCustomer = () => {
    // Cancelar muchas request

    setIsLoading(true);
    getCustomer({ token, search })
      .then((res) => {
        setCustomer(res);
        setIsLoading(false);
        searchFinantialAccounts();
        searchSubscribers();
      })
      .catch((e) => {
        setError(e.error_description);
        setIsLoading(false);
      })
      .finally(() => {
        //cancel.current = true;
      });
  };

  const searchFinantialAccounts = () => {
    getFinantialAccounts({ token, search })
      .then((res) => {
        setFinantialAccounts(res);
      })
      .catch((e) => {
        setError(e.error_description);
      })
      .finally(() => {
        //cancel.current = true;
      });
  };

  const searchSubscribers = () => {
    getSubscribers({ token, search })
      .then((res: ISubscriber[]) => {
        res[0].selected = true;
        setSubscribers(res);
      })
      .catch((e) => {
        setError(e.error_description);
      })
      .finally(() => {
        //cancel.current = true;
      });
  };

  return { customer, searchCustomer, isLoading, error };
}

export default useHookCustomer;
