import { useEffect, useMemo } from 'react';
import { useProfile } from '../store/profile';
import { useRouter } from 'next/router';

function useProtectedBTB() {
  const router = useRouter();
  const profile = useProfile();
  const isBTB: boolean = useMemo(
    () => foundIsBTB(profile?.bussiness),
    [profile?.bussiness]
  );
  
  useEffect(() => {
    if (isBTB) {
      router.push('/balance');
    }
  }, [isBTB]);

  return isBTB;
};

function foundIsBTB(type: string): boolean {
  return type === 'B2B' ? true : false
}

export default useProtectedBTB;