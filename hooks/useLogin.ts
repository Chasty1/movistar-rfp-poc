import { useState, useRef, useEffect } from "react";
import login, { ILogin } from "../service/login";
import { useRouter } from "next/router";
import { useSetToken } from "../store/user";

function useLogin({ username, password }: ILogin) {
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const router = useRouter();
  const setToken = useSetToken();
  let cancel = useRef(false);

  useEffect(() => {
    cancel.current = false;
  }, [username, password]);

  const handleLogin = () => {
    // Cancelar muchas request
    if (cancel.current === true) {
      return;
    }
    setIsLoading(true);
    login({ username, password })
      .then((res) => {
        const { token } = res;
        setToken(token);
        router.push("/");
        setIsLoading(false);
      })
      .catch((e) => {
        //alert(e);
        setError(e.message);
        setIsLoading(false);
      })
      .finally(() => {
        cancel.current = true;
      });
  };

  return { handleLogin, isLoading, error };
}

export default useLogin;
