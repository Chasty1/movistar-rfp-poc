import { useState, useRef, useEffect } from "react";
import login, { ILogin } from "../service/login";
import { useRouter } from "next/router";
import { useSetToken, useUser } from "../store/user";
import getCustomer from "../service/get-customer";
import {
  useCustomer,
  useSetCustomer,
  useSetFinantialAccounts,
  useSetSubscribers,
} from "../store/customer";
import decodeToken from "jwt-decode";
import getOffers from "../service/get-offers";
import { useSetOffers } from "../store/offer";
import replaceofferPromise from "../service/post-offer";
import { _storage } from "../utils/functions";

function useHookOffer() {
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const setOffers = useSetOffers();
  const router = useRouter();
  const { token } = useUser();

  const searchOffers = () => {
    getOffers({ token })
      .then((res) => {
        setOffers(res);
      })
      .catch(() => {})
      .finally(() => {});
  };

  useEffect(() => {
    searchOffers();
  }, []);

  return { isLoading, error };
}

export function useFetchReplaceOffer() {
  const [message, setMessage] = useState("");
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { token } = useUser();
  const customer = useCustomer();

  const saveOffer = () => {
    setIsLoading(true);
    replaceofferPromise({ customer_id: customer.id, item: 125, token })
      .then((res) => {
        setMessage(res.status);
        setIsLoading(false);
      })
      .catch((e) => {
        setMessage(e.status);
        setIsLoading(false);
      });
  };

  return { saveOffer, isLoading, message, setMessage };
}

export default useHookOffer;
