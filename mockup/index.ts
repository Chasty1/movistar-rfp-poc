export const users = [
  {
    id: 1,
    username: "Willy",
    email: "willy@everis.com",
    password: "123",
    userToken: "jwt-token1",
  },
  {
    id: 2,
    username: "Cristian",
    email: "cris@everis.com",
    password: "1234",
    userToken: "jwt-token2",
  },
];
