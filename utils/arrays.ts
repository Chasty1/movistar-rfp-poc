function groupByKeyAndToKey(items:any , keyFrom: string, keyTo: string) {
  let grouped = [];

  items.map((item:any) => {
    const valueToEvaluate = item[keyFrom];
    let foundedItem = grouped.find((item) => item[keyFrom] == valueToEvaluate);
    if (foundedItem) {
      foundedItem[keyTo].push(item);
    } else {
      grouped.push({
        [keyFrom]: valueToEvaluate,
        [keyTo]: [item],
      });
    }
  });

  return grouped;
}

export default groupByKeyAndToKey
