import { ILogin } from "../service/login";
import decodeToken from "jwt-decode";
import { ICustomerOffer } from "../service/post-offer";

const handleErrorBackButton = () => {
  history.go(-1);
};

export default handleErrorBackButton;

export const _withHeaders = (data: ILogin) => {
  return {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
  };
};

export const _withHeadersPost = (data: ICustomerOffer) => {
  return {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
      authorization: `Bearer ${data.token}`,
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
  };
};

export const _storage = {
  get: (key: string) => {
    if (typeof window !== "undefined") {
      try {
        let v;
        if (localStorage.getItem(key)) {
          v = JSON.parse(localStorage.getItem(key) || "");
          return v;
        }
        localStorage.removeItem(key);
        v = null;
        return v;
      } catch (e) {
        localStorage.removeItem(key);
        return null;
      }
    }
  },
};

export function verifyExpirationOfToken(token: string | null) {
  if (token) {
    const decodedToken = decodeToken(token);
    // @ts-ignore
    if (decodedToken.exp * 1000 < Date.now()) {
      localStorage.clear();
      return null;
    } else {
      return token;
    }
  } else return null;
}

export function generateNavItems(initialItems = [], isBtb = false) {
  let items;
  if (isBtb) {
    const potentialItem = initialItems.find((i) => i.href === "/about");
    items = initialItems.filter((i) => i.href !== potentialItem.href);
    return items;
  }
  return initialItems;
}
