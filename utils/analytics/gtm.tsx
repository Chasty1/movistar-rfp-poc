import React, { useEffect } from "react";
/**
 * GTM code snippets must be previously injected through _document.tsx
 * in Next.js App.
 * @see GTM Docs
 * @link https://developers.google.com/tag-manager/quickstart?hl=es *
 */
interface ScreenViewProps {
    event: string;
    pageName: string;
}
interface ScreenEventProps {
    event: string;
    eventCategory: string;
    eventAction: string;
    eventLabel?: string;
    eventValue?: number;
}
export type EventObject = ScreenEventProps | ScreenViewProps;

function pushGTMEvent(event: EventObject) {
    const eventInstance = { ...event };
    //@ts-ignore
    window && window.dataLayer && window.dataLayer.push(eventInstance);
}

/**
 * Triggers new GTM event on component first render.
 * @param {EventObject} event GTM's triggered event
 */
function useGTMEventEffect(event: EventObject) {
    useEffect(() => {
        pushGTMEvent(event);
    }, []);
}

/**
 * Adds click event listeners to elements on first render and pushes event to dataLayer when elment is clicked.
 *
 * To connect this method to any element, add _id_ attribute to that element and pass that id as second argument.
 * @param {EventObject} event GTM's triggered event
 * @param {string } elementId tracked elements'id
 */
function pushGTMEventOnClick(event: EventObject, elementId: string) {
    useEffect(() => {
        const linkButton = document.getElementById(elementId);
        linkButton.addEventListener("click", () => pushGTMEvent(event));
        return () => {
            linkButton.removeEventListener("click", () => pushGTMEvent(event));
        };
    }, []);
}

/**
 * Injects GTM event trigger to React functional Component.
 * GTM event is triggered on first render only.
 * @param {React.FC} Component HOC's Wrapped Component
 * @param {EventObject} event GTM's triggered event
 */
function withGTMEvent(Component: React.FC<{}>, event: EventObject) {
    const WrappedComponent = (props?: any) => {
        useGTMEventEffect(event);
        return <Component {...props} />;
    };
    WrappedComponent.displayName = Component.name;
    return WrappedComponent;
}

export { useGTMEventEffect, withGTMEvent, pushGTMEvent, pushGTMEventOnClick };
export { SCREEN_VIEWS, SCREEN_EVENTS } from "./gtm-consts";
export { GTM_ID } from "../consts";
