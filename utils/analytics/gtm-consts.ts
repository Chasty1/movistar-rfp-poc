export let SCREEN_VIEWS = {
    saldo: {
        event: "screenView",
        pageName: "saldo",
    },
    saldo_ultimos_movimientos_de_saldo: {
        event: "screenView",
        pageName: "saldo_ultimos_movimientos_de_saldo",
    },
    recargar: {
        event: "screenView",
        pageName: "saldo_recargas",
    },
    multiplicar: {
        event: "screenView",
        pageName: "saldo_multiplicar",
    },
    home_recargas: {
        event: "screenView",
        pageName: "home recargas",
    },
    sos_monto: {
        event: "screenView",
        pageName: "recargas sos monto",
    },
    sos_resumen: {
        event: "screenView",
        pageName: "recargas sos resumen",
    },
    sos_pendiente: {
        event: "screenView",
        pageName: "recargas sos pendiente",
    },
    sos_success: {
        event: "screenView",
        pageName: "recargas sos gracias",
    },
    tc_td_resumen: {
        event: "screenView",
        pageName: "recargas tc-td resumen",
    },
    tc_td_success: {
        event: "screenView",
        pageName: "recargas tc-td gracias",
    },
    mercado_pago_resumen: {
        event: "screenView",
        pageName: "recargas dinero en cuenta MP resumen",
    },
    mercado_pago_success: {
        event: "screenView",
        pageName: "recargas dinero en cuenta MP gracias",
    },
    multiplicate_home: {
        event: "screenView",
        pageName: "multiplicate home",
    },
    multiplicate_resumen: {
        event: "screenView",
        pageName: "multiplicate resumen",
    },
    multiplicate_gracias: {
        event: "screenView",
        pageName: "multiplicate gracias",
    },
    multiplicate_error: {
        event: "screenView",
        pageName: "multiplicate_error_",
    },
};

export let SCREEN_EVENTS = {
    ver_detalles_de_saldo: {
        event: "dashboard",
        eventCategory: "dashboard",
        eventAction: "saldo",
        eventLabel: "ver detalles de saldo",
    },
    ver_ultimos_movimientos: {
        event: "saldo",
        eventCategory: "saldo",
        eventAction: "ver detalles de saldo",
        eventLabel: "ultimos movimientos",
    },
    recargar: {
        event: "saldo",
        eventCategory: "saldo",
        eventAction: "detalles de saldo",
        eventLabel: "recargar",
    },
    multiplicar: {
        event: "saldo",
        eventCategory: "saldo",
        eventAction: "detalles de saldo",
        eventLabel: "multiplicar",
    },
    sos_recargar: {
        event: "recargas",
        eventCategory: "recargas",
        eventAction: "recargas sos",
    },
    sos_seleccionar_monto: {
        event: "recargas",
        eventCategory: "recargas",
        eventAction: "recargas sos",
        eventLabel: "monto seleccionado",
        eventValue: "",
    },
    sos_confirmar: {
        event: "recargas",
        eventCategory: "recargas",
        eventAction: "recargas sos",
        eventLabel: "recarga",
    },
    sos_gracias_volver: {
        event: "recargas",
        eventCategory: "recargas",
        eventAction: "recargas sos",
        eventLabel: "volver a mi línea gracias",
    },
    sos_transaccion: {
        event: "transaccion",
        eventCategory: "transaccion",
        eventAction: "reargas",
        eventLabel: "recargas sos",
        eventValue: "",
    },
    sos_error_volver: {
        event: "recargas",
        eventCategory: "recargas",
        eventAction: "recargas sos",
        eventLabel: "volver a mi línea error",
    },
    sos_recarga_pendiente: {
        event: "recargas",
        eventCategory: "recargas",
        eventAction: "recargas sos",
        eventLabel: "Recarga Pendiente",
        eventValue: "",
    },
    tc_td_continuar: {
        event: "recargas",
        eventCategory: "recargas",
        eventAction: "recargas tc-td",
        eventLabel: "continuar",
        eventValue: "",
    },
    tc_td_gracias_volver: {
        event: "recargas",
        eventCategory: "recargas",
        eventAction: "recargas tc-td",
        eventLabel: "volver a mi linea",
    },
    tc_td_error_volver: {
        event: "recargas",
        eventCategory: "recargas",
        eventAction: "recargas tc-td",
        eventLabel: "volver a mi linea error",
    },
    tc_td_transaccion: {
        event: "transaccion",
        eventCategory: "transaccion",
        eventAction: "recargas",
        eventLabel: "recargas tc-td",
        eventValue: "",
    },
    mercado_pago_continuar: {
        event: "recargas",
        eventCategory: "recargas",
        eventAction: "recargas mercado pago",
        eventLabel: "continuar",
        eventValue: "",
    },
    mercago_pago_gracias_volver: {
        event: "recargas",
        eventCategory: "recargas",
        eventAction: "recargas dinero en cuenta MP",
        eventLabel: "volver a mi linea gracias",
    },
    mercado_pago_error_volver: {
        event: "recargas",
        eventCategory: "recargas",
        eventAction: "recargas dinero en cuenta MP",
        eventLabel: "volver a mi linea error",
    },
    mercado_pago_transaccion: {
        event: "transaccion",
        eventCategory: "transaccion",
        eventAction: "recargas",
        eventLabel: "recargas dinero en cuenta MP",
        eventValue: "",
    },
    multiplicate_seleccion: {
        event: "multiplicate",
        eventCategory: "multiplicate",
        eventAction: "seleccion de paquete",
        eventLabel: "",
        eventValue: "",
    },
    multiplicate_multiplica: {
        event: "multiplicate",
        eventCategory: "multiplicate",
        eventAction: "multiplica",
        eventLabel: "",
        eventValue: "",
    },
    multiplicate_volver: {
        event: "multiplicate",
        eventCategory: "multiplicate",
        eventAction: "volver a mi linea gracias",
    },
    multiplicate_transaccion: {
        event: "transaccion",
        eventCategory: "transaccion",
        eventAction: "multiplicate",
        eventLabel: "",
        eventValue: "",
    },
    multiplicate_volver_error: {
        event: "multiplicate",
        eventCategory: "multiplicate",
        eventAction: "volver a mi linea error",
    },
};
