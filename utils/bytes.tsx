export const transformBytesToGb = (bytes: number) => {
    if(bytes >= 0){
        let gigaBytes = (bytes/1073741824);
        if((gigaBytes % 1 == 0) || (gigaBytes < 1)){
            return Math.round(gigaBytes).toString()
        }else{
            return gigaBytes.toFixed(2);
        }
    }else {
        return "dataInvalid";
    }
}