import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import API_URI from "./consts"

export function parseURL(
  url: string,
  path: string,
  params: object,
  query_params?: object
): string {
  let newURL = new URL(url)
  const key_values = Object.entries(params)
  const parsedPath = key_values.reduce(
    (acc, act) => acc.replace(`{${act[0]}}`, act[1]),
    path
  )
  newURL.pathname = parsedPath
  if (query_params) {
    Object.entries(query_params).forEach(([key, value]) =>
      newURL.searchParams.set(key, value)
    )
  }
  return newURL.toString()
}

const fetchApiEffect = (uri: string, opts?) => {
  let [response, setResponse] = useState({
    error: null,
    data: null,
    loading: true,
  })

  const router = useRouter()
  let { contextAni } = router.query

  useEffect(() => {
    if (contextAni) {
      fetch(
        parseURL(API_URI, uri, {
          user_id: "void",
          phone_number: contextAni,
        }),
        opts ? opts : null
      )
        .then((res) => {
          if (res.status === 200) {
            return res.json()
          } else {
            throw res
          }
        })
        .then((res) => {
          setResponse({ ...response, loading: false, data: res })
        })
        .catch((error) => {
          setResponse({ ...response, loading: false, error: error })
        })
    }
  }, [router])

  return response
}

export { fetchApiEffect }

const fetchApiEffectWithSessionStorage = (uri: string, opts?) => {
  let [response, setResponse] = useState({
    error: null,
    data: null,
    cache: null,
    loading: true,
  })

  const router = useRouter()
  let { contextAni } = router.query

  useEffect(() => {
    if (contextAni) {
      const sessionCache = JSON.parse(
        sessionStorage.getItem(
          "movistar." +
            parseURL(API_URI, uri, {
              user_id: "void",
              phone_number: contextAni,
            })
        )
      )
      if (sessionCache) {
        setResponse({ ...response, cache: sessionCache })
      }
      fetch(
        parseURL(API_URI, uri, {
          user_id: "void",
          phone_number: contextAni,
        }),
        opts ? opts : null
      )
        .then((res) => {
          if (res.status === 200) {
            return res.json()
          } else {
            throw res
          }
        })
        .then((res) => {
          sessionStorage.setItem(
            "movistar." +
              parseURL(API_URI, uri, {
                user_id: "void",
                phone_number: contextAni,
              }),
            JSON.stringify(res)
          )
          setResponse({ ...response, loading: false, data: res })
        })
        .catch((error) => {
          setResponse({ ...response, loading: false, error: error })
        })
    }
  }, [router])

  return response
}

export { fetchApiEffectWithSessionStorage }

const fetchApiEffectWithLocalStorage = (uri: string, opts?) => {
  let [response, setResponse] = useState({
    error: null,
    data: null,
    cache: null,
    loading: true,
  })

  const router = useRouter()
  let { contextAni } = router.query

  useEffect(() => {
    if (contextAni) {
      const localCache = JSON.parse(
        localStorage.getItem(
          "movistar." +
            parseURL(API_URI, uri, {
              user_id: "void",
              phone_number: contextAni,
            })
        )
      )
      if (localCache) {
        setResponse({ ...response, cache: localCache })
      }
      fetch(
        parseURL(API_URI, uri, {
          user_id: "void",
          phone_number: contextAni,
        }),
        opts ? opts : null
      )
        .then((res) => {
          if (res.status === 200) {
            return res.json()
          } else {
            throw res
          }
        })
        .then((res) => {
          localStorage.setItem(
            "movistar." +
              parseURL(API_URI, uri, {
                user_id: "void",
                phone_number: contextAni,
              }),
            JSON.stringify(res)
          )
          setResponse({ ...response, loading: false, data: res })
        })
        .catch((error) => {
          setResponse({ ...response, loading: false, error: error })
        })
    }
  }, [router])

  return response
}

export { fetchApiEffectWithLocalStorage }
