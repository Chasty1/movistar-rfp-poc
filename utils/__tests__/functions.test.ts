import handleErrorBackButton from "../functions"

const mockHistory = {go: jest.fn()}

it("handleBackButton call mock", () => {
    //@ts-ignore
    global.history = mockHistory
    handleErrorBackButton()
    expect(mockHistory.go).toHaveBeenCalled()
})

it("handeBackButton have 'history.go'", () => {
    expect(handleErrorBackButton.toString()).toContain("history.go")
})
