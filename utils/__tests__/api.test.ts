import { fetchApiEffect } from "../api";

describe("fetchApi functions", () => {
  describe("fetchApiEffect function", () => {
    test("should be a function", () => {
      expect(typeof fetchApiEffect).toBe("function");
    });
  });
});
