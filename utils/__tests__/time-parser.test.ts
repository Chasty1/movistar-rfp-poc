import { parseDate, parseTime } from "../time-parser"

test("Day < 9 is parsed to dd/mm/yyyy format", () => {
  const date = new Date("2020-12-01 07:31:26")
  expect(parseDate(date)).toBe("01/12/2020")
})

test("Month < 9 is parsed to dd/mm/yyyy format", () => {
  const date = new Date("2020-07-21 07:31:26")
  expect(parseDate(date)).toBe("21/07/2020")
})

test("Month and Day < 9 are parsed to dd/mm/yyyy", () => {
  const date = new Date("2020-07-08 07:31:26")
  expect(parseDate(date)).toBe("08/07/2020")
})

test("Two digits month AND day parsed to dd/mm/yyyy", () => {
  const date = new Date("2020-12-21 07:31:26")
  expect(parseDate(date)).toBe("21/12/2020")
})

test("Hour < 9 is parsed to hh:mm format", () => {
  const date = new Date("2020-12-01 07:31:26")
  expect(parseTime(date)).toBe("07:31")
})

test("Minute < 9 is parsed to hh:mm format", () => {
  const date = new Date("2020-07-21 07:01:26")
  expect(parseTime(date)).toBe("07:01")
})

test("Minutes and Day < 9 are parsed to hh:mm", () => {
  const date = new Date("2020-07-08 07:01:26")
  expect(parseTime(date)).toBe("07:01")
})

test("Two digits Minutes AND Day parsed to hh:mm", () => {
  const date = new Date("2020-12-21 17:31:26")
  expect(parseTime(date)).toBe("17:31")
})
