const API_URI = process.env.NEXT_PUBLIC_API_URI;

export const GTM_ID = { test: "GTM-P3VJ4DV", prod: "GTM-5NDT24D" };

export const BASE_ICONS_URI = "/assets/icons";

export const endpoint = {
  API_RFP_AUTH: "http://localhost:3000",
  //API_RFP_AUTH : 'http://poc-git-muzychuks87-dev.apps.sandbox.x8i5.p1.openshiftapps.com'
};

export const navItems = [
  {
    href: "/balance",
    tabText: "Cuenta",
    iconPathOn: "/assets/icons/navbar/icn-nav-account-on.svg",
    iconPathOff: "/assets/icons/navbar/icn-nav-account-off.svg",
  },
  {
    href: "/about",
    tabText: "Descubrí",
    iconPathOn: "/assets/icons/navbar/icn-nav-explore-on.svg",
    iconPathOff: "/assets/icons/navbar/icn-nav-explore-off.svg",
  },
  {
    href: "/notifications",
    tabText: "Notificaciones",
    iconPathOn: "/assets/icons/navbar/icn-nav-notifications-on.svg",
    iconPathOff: "/assets/icons/navbar/icn-nav-notifications-off.svg",
  },
  {
    href: "/profile",
    tabText: "Perfil",
    iconPathOn: "/assets/icons/navbar/icn-nav-profile-on.svg",
    iconPathOff: "/assets/icons/navbar/icn-nav-profile-off.svg",
  },
];

export default API_URI;
