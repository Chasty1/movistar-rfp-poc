
const transformNumberToCurrency = ( value: number ) => {
    if(value >= 0){
        return `$${ value.toFixed(2).replace(".",",") }`
    } else {
        return `-$${ ( value * -1 ).toFixed(2).replace(".",",") }`
    }
}


export default transformNumberToCurrency