/**
 * Takes a Date object type and returns date string in **dd/mm/yyyy** format.
 * @param date Date object
 */

function parseDate(date: Date) {
  let day: string, month: string, year: string

  day = ("0" + date.getDate()).slice(-2)
  month =("0" + (date.getMonth() + 1)).slice(-2)
  year = `${date.getFullYear()}`
  
  const dateParsed = `${day}/${month}/${year}`
  
  return dateParsed
}

function parseTime(time: Date){
  let hours: string, minutes: string
  
  hours = ("0" + time.getHours()).slice(-2)
  minutes =("0" + time.getMinutes()).slice(-2)

  const timeParsed = `${hours}:${minutes}`

  return timeParsed
}

export { parseDate, parseTime }




