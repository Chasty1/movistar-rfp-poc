import { useLayoutEffect, FC, useState } from "react";
import { useRouter } from "next/router";
import { useUser } from "../store/user";

export default function withAuth(ProtectedComponent: FC) {
  return (props) => {
    const { token } = useUser();
    const [isAuth, setIsAuth] = useState<boolean>(false);
    const router = useRouter();

    useLayoutEffect(() => {
      if (!token) {
        router.push("/login");
      } else {
        setIsAuth(true);
      }
    }, [token]);

    return isAuth ? <ProtectedComponent {...props} /> : null;
  };
}
