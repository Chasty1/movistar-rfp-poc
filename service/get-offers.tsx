import { REFUSED } from "dns";
import { IOffer, ISubscriber } from "../store/types";

import { endpoint } from "../utils/consts";

type Params = {
  token: string | null;
};

// Datos de respuesta de la simulacion del servicio de perfilamiento
// (para implementaciones reales, modificar este Type)

export default async function getOffers({ token }: Params): Promise<IOffer[]> {
  if (!token) {
    throw new Error(`El token  debe proveerse`);
  }

  let fail: boolean = true;
  return window
    .fetch(`${endpoint.API_RFP_AUTH}/apis/api.rodar/replace_offer/`, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    })
    .then((res) => {
      if (!res.ok) {
        fail = false;
      }
      return res.json();
    })
    .then((resJson) => {
      if (!fail) {
        throw resJson;
      }

      return resJson["compras"]["items"] as Promise<IOffer[]>;
    })
    .catch((e) => {
      throw e;
    });
}
