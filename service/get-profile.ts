import { REFUSED } from "dns";
import { Identities, IRepresentante } from "../store/types";
import { endpoint } from "../utils/consts";

import decodeToken from "jwt-decode";
import { _storage } from "../utils/functions";

type Params = {
  token: string | null;
};

// Datos de respuesta de la simulacion del servicio de perfilamiento
// (para implementaciones reales, modificar este Type)
type IProfileResponse = {
  id: number;
  name: string;
  email: string;
  context_ani: number;
  customer_value?: string;
  suscriptor?: string;
  bussiness?: string;
  identities: Identities[];
};

export default async function getProfile({
  token,
}: Params): Promise<IRepresentante> {
  if (!token) {
    throw new Error(`El token  debe proveerse`);
  }

  // @ts-ignore
  const repId = decodeToken(_storage.get("access_token")).repid;

  let fail: boolean = true;
  return window
    .fetch(`${endpoint.API_RFP_AUTH}/apis/api.rodar/reps/${repId}`, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    })
    .then((res) => {
      if (!res.ok) {
        fail = false;
      }
      return res.json();
      //return res.json()["representantes"][0] as Promise<IRepresentante>;
    })
    .then((resJson) => {
      if (!fail) {
        throw resJson;
      }

      return resJson["representantes"][0] as Promise<IRepresentante>;
    })
    .catch((e) => {
      throw e;
    });
}
