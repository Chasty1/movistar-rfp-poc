import { REFUSED } from "dns";
import { ISubscriber } from "../store/types";

import { endpoint } from "../utils/consts";

type Params = {
  token: string | null;
  search: string;
};

// Datos de respuesta de la simulacion del servicio de perfilamiento
// (para implementaciones reales, modificar este Type)

export default async function getSubscribers({
  token,
  search,
}: Params): Promise<ISubscriber[]> {
  if (!token) {
    throw new Error(`El token  debe proveerse`);
  }

  let fail: boolean = true;
  return window
    .fetch(`${endpoint.API_RFP_AUTH}/apis/api.rodar/subscribers/${search}`, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    })
    .then((res) => {
      if (!res.ok) {
        fail = false;
      }
      return res.json();
    })
    .then((resJson) => {
      if (!fail) {
        throw resJson;
      }

      return resJson["subscribers"]["subscribers"] as Promise<ISubscriber[]>;
    })
    .catch((e) => {
      throw e;
    });
}
