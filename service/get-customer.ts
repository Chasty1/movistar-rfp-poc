import { REFUSED } from "dns";
import { ICustomer, Identities, IRepresentante } from "../store/types";
import { endpoint } from "../utils/consts";

type Params = {
  token: string | null;
  search: string;
};

// Datos de respuesta de la simulacion del servicio de perfilamiento
// (para implementaciones reales, modificar este Type)

export default async function getCustomer({
  token,
  search,
}: Params): Promise<ICustomer> {
  if (!token) {
    throw new Error(`El token  debe proveerse`);
  }

  let fail: boolean = true;
  return window
    .fetch(`${endpoint.API_RFP_AUTH}/apis/api.rodar/customers/${search}`, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    })
    .then((res) => {
      if (!res.ok) {
        fail = false;
      }
      return res.json();
    })
    .then((resJson) => {
      if (!fail) {
        throw resJson;
      }

      return resJson["customers"][0] as Promise<ICustomer>;
    })
    .catch((e) => {
      throw e;
    });
}
