import { REFUSED } from "dns";
import {
  ICustomer,
  Identities,
  IFinantialAccount,
  IRepresentante,
} from "../store/types";
import { endpoint } from "../utils/consts";

type Params = {
  token: string | null;
  search: string;
};

// Datos de respuesta de la simulacion del servicio de perfilamiento
// (para implementaciones reales, modificar este Type)

export default async function getFinantialAccounts({
  token,
  search,
}: Params): Promise<IFinantialAccount[]> {
  if (!token) {
    throw new Error(`El token  debe proveerse`);
  }

  let fail: boolean = true;
  return window
    .fetch(
      `${endpoint.API_RFP_AUTH}/apis/api.rodar/finantial_accounts/${search}`,
      {
        headers: {
          authorization: `Bearer ${token}`,
        },
      }
    )
    .then((res) => {
      if (!res.ok) {
        fail = false;
      }
      return res.json();
    })
    .then((resJson) => {
      if (!fail) {
        throw resJson;
      }

      return resJson["cuentaCorriente"]["finantial_accounts"] as Promise<
        IFinantialAccount[]
      >;
    })
    .catch((e) => {
      throw e;
    });
}
