import { endpoint } from "../utils/consts";
import { _withHeaders, _withHeadersPost } from "../utils/functions";

export type ICustomerOffer = {
  customer_id: number;
  item: number;
  token: string;
};

// Datos de respuesta de Keycloack
type ResponseData = {
  status: string;
};

const replaceofferPromise = ({
  customer_id,
  item,
  token,
}: ICustomerOffer): Promise<ResponseData> => {
  let fail: boolean;
  return window
    .fetch(
      `${endpoint.API_RFP_AUTH}/apis/api.rodar/replace_offer`,
      _withHeadersPost({ customer_id, item, token })
    )
    .then((res) => {
      if (!res.ok) {
        fail = true;
      }
      if (res.status == 404) {
        fail = true;
        return res.json();
      }

      return res.json();
    })
    .then((json) => {
      if (fail) {
        throw json;
      }
      return json;
    })
    .catch((e) => {
      throw e;
    });
};

export default replaceofferPromise;
