import { endpoint } from "../utils/consts";
import { _withHeaders } from "../utils/functions";

export type ILogin = {
  username: string;
  password: string;
};

// Datos de respuesta de Keycloack
type ResponseData = {
  token: string;
};

const loginPromise = ({
  username,
  password,
}: ILogin): Promise<ResponseData> => {
  let fail: boolean;
  return window
    .fetch(
      `${endpoint.API_RFP_AUTH}/api/user/login`,
      _withHeaders({ username, password })
    )
    .then((res) => {
      if (!res.ok) {
        fail = true;
      }
      if (res.status == 404) {
        fail = true;
        return res.json();
      }

      return res.json();
    })
    .then((json) => {
      if (fail) {
        throw json;
      }
      return json;
    })
    .catch((e) => {
      throw e;
    });
};

export default loginPromise;
