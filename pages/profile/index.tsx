import React from "react";
import MainAppContainer from "../../components/common/main-app-container";
import { useLogout, useUserInfo } from "../../store/user";
import Button from "../../components/common/button";
import withAuth from "../../utils/withAuth";

const Profile = () => {
  const userInfo = useUserInfo();
  const logout = useLogout();
  return (
    <MainAppContainer>
      <div style={{ textAlign: "center" }}>
        <div style={{ marginBottom: 20, marginTop: 20 }}>
          <div
            style={{
              width: 80,
              height: 80,
              backgroundColor: "#E2E2E2",
              borderRadius: 80,
              margin: "auto",
            }}
          ></div>
          <p>{userInfo?.name}</p>
          <p>{userInfo?.rol}</p>
        </div>
        <div>
          <Button text="Salir" onClick={logout} />
        </div>
      </div>
    </MainAppContainer>
  );
};
export default withAuth(Profile);
