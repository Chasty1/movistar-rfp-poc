import { useEffect } from "react";
import { useRouter } from "next/router";
import withAuth from "../utils/withAuth";

const Index = () => {
  const router = useRouter();

  useEffect(() => {
    router.push("/home");
  }, []);

  return null;
};

export default withAuth(Index);
