import Head from "next/head";
import React from "react";
import Login from "../../components/common/login";

const LoginForm = () => {
  return (
    <>
      <Head>
        <title>Ingresá - Movistar Argentina</title>
      </Head>
      <Login />
    </>
  );
};

export default LoginForm;
