import Head from "next/head";
import { useRouter } from "next/router";
import React, {
  useCallback,
  useEffect,
  useMemo,
  useReducer,
  useState,
} from "react";
import MainAppContainer from "../../components/common/main-app-container";
import ActionsButtons from "../../components/common/ActionsButtons";
import CambioPlan from "../../components/common/icons/CambioPlan";
import CambioEquipo from "../../components/common/icons/CambioEquipo";
import Support from "../../components/common/icons/Support";
import Flag from "../../components/common/icons/Flag";
import Pelochos from "../../components/common/Pelochos";
import { useStateValue } from "../../context/state";

import withAuth from "../../utils/withAuth";
import { useUserInfo } from "../../store/user";
import Input from "../../components/common/input";
import useCustomer from "../../hooks/useCustomer";
import InfoBox from "../../components/common/info-box";
import SearchBar from "../../components/common/search-bar";
import Card from "../../components/common/card";
import CardInfo from "../../components/common/card-info";
import DropDown from "../../components/common/drop-down";
import StatusInfo from "../../components/common/status-info";
import { _storage } from "../../utils/functions";
import { useSelectedSubscriber } from "../../store/customer";
import { useOffers } from "../../store/offer";
import useFetchOffer, { useFetchReplaceOffer } from "../../hooks/useOffer";
import { IOffer } from "../../store/types";
import PopUp from "../../components/common/pop-up";
import ShowAlert from "../../components/common/show-alert";
import PromotionsButton from "../../components/common/buttonsPromotion/PromotionsButton";
import AltaCliente from "../../components/common/icons/AltaCliente";

const Cuenta = () => {
  let gigasPlan = 10;

  let gigas = 8;

  let gigasCalculo = Math.round((gigas / gigasPlan) * 100);

  let remain = gigasPlan - gigas;

  const userInfo = useUserInfo();

  const [inputValues, setInputValues] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    { search: "" }
  );

  const handleOnChange = (event) => {
    const { name, value } = event.target;
    setInputValues({ ...inputValues, [name]: value });
  };

  const { customer, searchCustomer } = useCustomer({
    ...inputValues,
  });

  const selectedSubscriber = useSelectedSubscriber();

  const [offer, setOffer] = useState<IOffer>();
  const offers = useOffers();
  useFetchOffer();

  useEffect(() => {
    if (selectedSubscriber) {
      const result = offers.find((x) => x.id === selectedSubscriber.offer_id);
      setOffer(result);
    } else {
      setOffer(null);
    }
  }, [offers, selectedSubscriber]);

  const [open, setOpen] = useState(false);

  const [showAlert, setShowAlert] = useState(false);
  const [alerMessage, setAlertMessage] = useState("");

  return (
    <>
      <Head>
        <title>Cuenta</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <MainAppContainer>
        <PopUp
          open={open}
          setOpen={setOpen}
          offerClicked={(message) => {
            setShowAlert(true);
            setAlertMessage(message);
            setTimeout(() => {
              setShowAlert(false);
            }, 2000);
          }}
        />
        <ShowAlert message={alerMessage} show={showAlert} />

        <div
          style={{
            display: "flex",
            backgroundColor: "transparent",
            flexDirection: "column",
            padding: 16,
          }}
        >
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              backgroundColor: "transparent",
              marginBottom: 10,
            }}
          >
            <SearchBar
              id="search"
              onChange={(e) => handleOnChange(e)}
              iconPath="/assets/icons/loupe.svg"
              type="text"
              placeholder="Busca el cliente por ANI o DNI"
              name="search"
              iconBgColor="#777"
              onClickIcon={(e) => searchCustomer()}
            />
            <div style={{ display: "flex", alignItems: "center" }}>
              <p style={{ margin: 0 }}>Id representante</p>
              <InfoBox
                text={userInfo.name}
                iconPath="/assets/icons/man.svg"
                iconBgColor="#333"
              />
            </div>
          </div>

          <Card style={{ borderRadius: "10px" }}>
            <InfoBox
              label="Nombre cliente"
              text={customer?.name ? customer.name : "No existen datos"}
              iconPath="/assets/icons/man.svg"
              iconBgColor="#333"
            />

            <InfoBox
              label="Tipo cliente"
              text={
                customer?.customer_type
                  ? customer.customer_type
                  : "No existen datos"
              }
              iconPath="/assets/icons/user.svg"
              iconBgColor="#333"
            />

            {userInfo.rol === "Callcenter" && (
              <InfoBox
                label="Cant Cuenta financiera"
                text={
                  customer?.finantial_accounts
                    ? customer?.finantial_accounts.length.toString()
                    : "0"
                }
                iconPath="/assets/icons/bank-building.svg"
                iconBgColor="#333"
                style={{ width: 90, textAlign: "center", top: -15 }}
              />
            )}

            <InfoBox
              label="Scoring"
              text={
                customer?.customer_score
                  ? customer.customer_score.toString()
                  : "0"
              }
              iconPath="/assets/icons/check.svg"
              iconBgColor="#5FC705"
            />

            <InfoBox
              label="Datos contacto"
              text=""
              iconPath="/assets/icons/remove.svg"
              iconBgColor="#ec6839"
              style={{ width: 40, textAlign: "center", top: -15 }}
            />

            <InfoBox
              label="Deuda Total"
              text={"Sin deuda"}
              iconPath="/assets/icons/dollar.svg"
              rightButton="Ver"
              iconBgColor="white"
              style={{ marginLeft: 26 }}
            />

            <InfoBox
              label="No Totalizado"
              text=""
              iconPath="/assets/icons/remove.svg"
              iconBgColor="#ec6839"
              style={{
                width: 40,
                textAlign: "center",
                top: -15,
              }}
            />

            <InfoBox
              label="NPS ultimo contacto"
              text={"Sin deuda"}
              iconPath="/assets/icons/dollar.svg"
              iconBgColor="white"
              style={{ marginLeft: 30 }}
            />
          </Card>

          {/* refactor cards */}
          <Card
            style={{
              flexDirection: "column",
              marginTop: 15,
              borderRadius: "10px",
            }}
          >
            <div style={{ display: "flex", marginBottom: 16 }}>
              <DropDown subscribers={customer.subscribers}></DropDown>
              <StatusInfo active label="Ordenes pendientes" value="0" />
              <StatusInfo
                label="Status de la linea"
                active={selectedSubscriber?.status == "actived"}
                value={
                  selectedSubscriber
                    ? selectedSubscriber.status
                    : "sin información"
                }
              />
              <StatusInfo
                label="Cuenta financiera"
                active={selectedSubscriber?.status == "actived"}
                value={
                  selectedSubscriber
                    ? selectedSubscriber.id_fa
                    : "sin información"
                }
              />
              <StatusInfo label="Tiene bonificaciones" value="0" />
              <StatusInfo label="Medida de innovar" value="0" />
            </div>

            <h3>
              {offer
                ? `${offer.name} ($ ${offer.price})`
                : "Plan: No se encontraron datos"}
            </h3>
            {/*section pelochos y cierre de ciclo*/}
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
                marginTop: 32,
              }}
            >
              <div style={{ display: "flex" }}>
                <Pelochos
                  value={gigasCalculo}
                  text_primary="Datos sin usar"
                  text_total_plan={`de 10 GB`}
                  text_secondary={`${String(remain)} GB`}
                  text_footer="Datos"
                />

                <Pelochos
                  value={100}
                  text_secondary={`Ilimitado`}
                  text_footer="Minutos otras campañas"
                />

                <Pelochos
                  value={100}
                  text_secondary={`Ilimitado`}
                  text_footer="SMS"
                />

                <Pelochos
                  value={100}
                  text_secondary={`Ilimitado`}
                  text_footer="Minutos movistar"
                />
              </div>

              <CardInfo
                title="Cierre de ciclo (c.5)"
                date="13/03"
                fechaFactura="Vto ultima factura 27/02"
                deuda="0"
              />

              <InfoBox
                label=""
                text=""
                iconPath="/assets/icons/alarm-clock.svg"
                iconBgColor="#ec6839"
                style={{
                  width: 40,
                  textAlign: "center",
                  top: -15,
                  marginRight: 20,
                }}
              />
            </div>

            <div style={{ marginTop: 16 }}>
              <h4>Tramites</h4>
              <div
                style={{
                  display: "flex",
                  borderTopWidth: 5,
                  marginTop: 15,
                  backgroundColor: "transparent",
                  justifyContent: "space-between",
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexWrap: "wrap",
                    alignItems: "flex-start",
                    width: "100%",
                  }}
                >
                  <ActionsButtons
                    onClick={(e) => {
                      if (selectedSubscriber) {
                        setOpen(true);
                      }
                    }}
                    text="Cambio de plan"
                    icon={() => <CambioPlan />}
                  />

                  <ActionsButtons
                    onClick={(e) => {}}
                    text="Cambio de titularidad"
                    icon={() => <Support />}
                  />

                  {userInfo.rol === "Retencion" && (
                    <ActionsButtons
                      onClick={(e) => {}}
                      text="Baja de linea"
                      icon={() => <CambioPlan />}
                    />
                  )}
                  {userInfo.rol === "Retencion" && (
                    <ActionsButtons
                      onClick={(e) => {}}
                      text="Suspención por robo"
                      icon={() => <CambioEquipo />}
                    />
                  )}
                </div>

                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    width: "55%",
                  }}
                >
                  <p style={{ margin: "6px 0" }}>Promociones disponibles</p>
                  <PromotionsButton color="purple" text="Totalización Hogar" />
                  <PromotionsButton color="green" text="Familia Movistar" />
                  <PromotionsButton color="red" text="Alta Fibra" />
                </div>
              </div>
            </div>
          </Card>

          <Card
            style={{
              marginTop: 15,
              borderRadius: "10px",
            }}
          >
            <div style={{}}>
              <h4>Tramites por DNI</h4>
              <div
                style={{
                  display: "flex",
                  borderTopWidth: 5,
                  marginTop: 15,
                  backgroundColor: "transparent",
                }}
              >
                <ActionsButtons
                  onClick={(e) => {}}
                  text="Alta de linea"
                  icon={() => <AltaCliente />}
                />

                <ActionsButtons
                  onClick={(e) => {}}
                  text="Potabilidad"
                  icon={() => <CambioEquipo />}
                />
              </div>
            </div>
          </Card>
        </div>
      </MainAppContainer>
    </>
  );
};

export default withAuth(Cuenta);
