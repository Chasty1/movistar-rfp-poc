import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import MainAppContainer from "../../components/common/main-app-container";
import { useStateValue } from "../../context/state";
import withAuth from "../../utils/withAuth";

const Notifications = () => {
  return (
    <>
      <Head>
        <title>Notificaciones</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <MainAppContainer>
        <h1>Notificaciones</h1>
      </MainAppContainer>
    </>
  );
};

export default withAuth(Notifications);
