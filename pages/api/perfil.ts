import { NextApiRequest, NextApiResponse } from "next";
import decodeJwt from "jwt-decode";
import { __mock_profile__ } from "../../__mocks__/profile-data";

export default (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method === "GET") {
    const authHeader = req.headers.authorization;

    if (authHeader) {
      const token = authHeader.split("Bearer ")[1];
      if (token) {
        try {
          const user = decodeJwt(token);
          const profileData = __mock_profile__.find(
            // @ts-ignore
            (p) => p.email === user.email
          );
          if (!profileData) {
            res
              .status(404)
              .json({ message: "No se encontraron los datos del perfil" });
          }
          res.json(profileData);
        } catch (e) {
          console.log(e);
          res.status(404).json({ message: "El token es inválido o expiró" });
        }
      } else {
        res
          .status(400)
          .json({ message: "El formato del header debe ser 'Bearer [token]'" });
      }
    } else {
      res
        .status(400)
        .json({ message: "El header de autorización debe proveerse" });
    }
  }
};
