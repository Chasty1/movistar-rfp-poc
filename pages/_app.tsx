import "./styles.css";
import store from "../store";
import { _storage } from "../utils/functions";
import { verifyExpirationOfToken } from "../utils/functions";

const { StoreProvider } = store;

export default function MyApp({ Component, pageProps }) {
  const initialState = {
    profile: {},
    user: {
      token: verifyExpirationOfToken(_storage.get("access_token")),
    },
    customer: {},
    offers: [],
  };
  return (
    <StoreProvider initialState={initialState}>
      <Component {...pageProps} />
    </StoreProvider>
  );
}
